#include "libhack.h"
#include <random>
#include <set>
#include <SDL.h>
#undef main

using namespace libhack;

class Character : public MapUnit
{
public:
	Character(Point at, Glyph g, Map *map) : MapUnit(at, g, map)
	{
		blockers = CollisionChannels::All();
		movement_type = 1;
	}
};

class PlayerCharacter : public Character
{
public:
	PlayerCharacter(Point at, Glyph g, Map *map) : Character(at, g, map) {}

	void CantMove(Point to) {}
};

TerrainInfo terrain_rock(Glyph(), CollisionChannels::All());
TerrainInfo terrain_floor(Glyph('.'), CollisionChannels::None());
TerrainInfo terrain_corridor(Glyph('#'), CollisionChannels::None());
TerrainInfo terrain_secret_corridor(Glyph('#', Green), CollisionChannels::None());
TerrainInfo terrain_upstair(Glyph('<'), CollisionChannels::None());
TerrainInfo terrain_downstair(Glyph('>'), CollisionChannels::None());
TerrainInfo terrain_wall_vertical(Glyph('|'), CollisionChannels::All());
TerrainInfo terrain_wall_horizontal(Glyph('-'), CollisionChannels::All());
TerrainInfo terrain_door(Glyph('+', Yellow), CollisionChannels::All());
TerrainInfo terrain_open_door(Glyph('-', Yellow), CollisionChannels::None());

enum TerrainType
{
	FLOOR,
	WALL,
	CORRIDOR,
};

class Level
{
public:
	Level() : map(CreateMap(80, 23)) {
		rects.push_back(Rect(Point(1,1), Point(79, 22)));
	}

	Point upstair() { return upstair_point; }
	Point downstair() { return downstair_point; }

	operator Map*() { return map; }

protected:
	std::vector<Rect> rects;
	Map *map;
	Point upstair_point, downstair_point;
};

constexpr size_t kMaxNumRooms = 12;
constexpr int kMaxRoomWidth = 16;
constexpr int kMaxRoomHeight = 12;

typedef std::function<int(int low, int high)> RndFn;

class MapGenerator
{
public:
	MapGenerator(std::mt19937& rng, Rect rect) : rng(rng), rect(rect)
	{
		AddRect(rect);
		tiles.resize(rect.size.width * rect.size.height);

		room_minimum_size = Size(6,6);
		room_maximum_size = Size(20, 16);
	}

	enum TileType
	{
		WALL = 0, DOOR, CORRIDOR, ROOM_FLOOR, ROOM_HWALL, ROOM_VWALL, ROOM_WALL_CORNER,
	};
	struct Room
	{
		Rect rect;
		size_t link;

		Point random_point(const RndFn& fn)
		{
			return Point(fn(rect.left() + 1, rect.right() - 1), fn(rect.top() + 1, rect.bottom() - 1));
		}
		bool operator < (const Room& r) const { return rect.tl.x < r.rect.tl.x; }
	};

	void GenerateRooms(size_t max_rooms)
	{
		for(size_t tries = 0; tries < 100 && !rects.empty() && rooms.size() < max_rooms; tries++) {
			if(rooms.size() > 4 && !rn2(max_rooms - rooms.size()))
				break;
			CreateRandomRoom();
		}
		SortRooms();
		for(size_t i = 0; i < rooms.size(); i++) {
			for(auto pt : rooms[i].rect) {
				auto& t = tile(pt);
				if(pt.x == rooms[i].rect.left() || pt.x == rooms[i].rect.right()) {
					t = ROOM_VWALL;
				}
				else if(pt.y == rooms[i].rect.top() || pt.y == rooms[i].rect.bottom()) {
					t = ROOM_HWALL;
				}
				else {
					t = ROOM_FLOOR;
				}
			}
		}
	}

	void GenerateCorridors()
	{
		for(size_t i = 0; i < rooms.size() - 1; i++) {
			JoinRooms(i, i + 1, false);
			if(!rn2(50))
				break;
		}
		for(size_t i = 0; i < rooms.size() - 2; i++) {
			if(rooms[i].link != rooms[i + 2].link)
				JoinRooms(i, i + 2, true);
		}
		bool any = true;
		for(size_t i = 0; i < rooms.size() && any; i++) {
			if(!rn2(rooms.size() + 4 - i))
				continue;
			any = false;
			for(size_t j = i + 1; j < rooms.size(); j++) {
				if(rooms[i].link != rooms[j].link) {
					JoinRooms(i, j, true);
					any = true;
				}
			}
		}
/*		if(rooms.size() > 2) {
			for(size_t i = rn2(rooms.size() - 1) + 4; i; i--) {
				size_t a = rn2(rooms.size() - 1);
				size_t b = rn2(rooms.size() - 3);
				if(b >= a) b += 2;
				JoinRooms(a, b, true);
			}
		}*/
	}

	const std::vector<TileType>& get_tiles() { return tiles; }

	TileType& tile(Point pt)
	{
		return tiles[(pt.x - rect.tl.x) + (pt.y - rect.tl.y) * rect.size.width];
	}

	void SortRooms()
	{
		std::sort(rooms.begin(), rooms.end());
	}

	bool CreateRandomRoom()
	{
		auto rect = rn2(rects);
		auto r = *rect;
		Point tl(rnd(0, r.size.width - room_minimum_size.width - 1), rnd(0, r.size.height - room_minimum_size.height - 1));
		Size sz(rnd(room_minimum_size.width, std::min(r.size.width - tl.x, room_maximum_size.width)),
			rnd(room_minimum_size.height, std::min(r.size.height - tl.y, room_maximum_size.height)));
		Rect room_rect(tl + r.tl, sz);

		if(!TryRoom(room_rect))
			return false;
		rects.erase(rect);
		SplitRects(r, room_rect);
		return true;
	}
	bool TryRoom(const Rect& room_rect)
	{
		Room room;

		room.rect = room_rect.add_margin(Size(-1,-1));
		room.link = rooms.size();

		if(room.rect.size.width < 4 || room.rect.size.height < 4)
			return false;
		rooms.push_back(room);
		return true;
	}


	void AddRect(const Rect& r)
	{
		if(r.size.width >= room_minimum_size.width && r.size.height >= room_minimum_size.height)
			rects.push_back(r);
	}

	void SplitRects(const Rect& src, const Rect& sub)
	{
		// Form
		//--|----|----|
		//  |rA  | rD |
		//  |----|    |
		//rB| sub|    |
		//  |----|    |
		//  | rC |    |
		//-------------

		Point tl = src.tl;
		Point br = src.tl + src.size;
		Point itl = sub.tl;
		Point ibr = sub.tl + sub.size;
		if(tl.x < itl.x) {
			// have a left side
			AddRect(Rect(tl, Size(itl.x - tl.x, src.size.height)));
		}
		if(br.x > ibr.x) {
			// have a right side
			AddRect(Rect(Point(ibr.x, tl.y), Size(br.x - ibr.x, src.size.height)));
		}
		if(tl.y < itl.y) {
			// top
			AddRect(Rect(Point(itl.x, tl.y), Size(ibr.x - itl.x, itl.y - tl.y)));
		}
		if(br.y > ibr.y) {
			// top
			AddRect(Rect(Point(itl.x, ibr.y), Size(ibr.x - itl.x, br.y - ibr.y)));
		}
	}

	bool okdoor(Point pt)
	{
		auto t = tile(pt);
		return t == ROOM_HWALL || t == ROOM_VWALL;
	}

	Point FindDoorPosition(Point a, Point b)
	{
		int x = rnd(a.x, b.x), y = rnd(a.y, b.y);
		if(okdoor(Point(x,y)))
			return Point(x,y);
		for(auto pt : Rect(a, b))
			if(okdoor(pt))
				return pt;
		for(auto pt : Rect(a, b))
			if(tile(pt) == DOOR)
				return pt;
		return a;
	}

	void JoinRooms(size_t a_idx, size_t b_idx, bool v)
	{
		if(a_idx == b_idx)
			return;
		auto& a = rooms[a_idx];
		auto& b = rooms[b_idx];

		Point dir;
		Point src, dst;
		if(a.rect.right() < b.rect.left()) {
			dir = Point(1, 0);
			int ax = a.rect.right();
			int bx = b.rect.left();
			src = FindDoorPosition(Point(ax, a.rect.top() + 1), Point(ax, a.rect.bottom() - 1));
			dst = FindDoorPosition(Point(bx, b.rect.top() + 1), Point(bx, b.rect.bottom() - 1));
		} else if(a.rect.top() > b.rect.bottom()) {
			dir = Point(0, -1);
			int ay = a.rect.top();
			int by = b.rect.bottom();
			src = FindDoorPosition(Point(a.rect.left() + 1, ay), Point(a.rect.right() - 1, ay));
			dst = FindDoorPosition(Point(b.rect.left() + 1, by), Point(b.rect.right() - 1, by));
		} else if(b.rect.right() < a.rect.left()) {
			dir = Point(-1, 0);
			int ax = a.rect.left();
			int bx = b.rect.right();
			src = FindDoorPosition(Point(ax, a.rect.top() + 1), Point(ax, a.rect.bottom() - 1));
			dst = FindDoorPosition(Point(bx, b.rect.top() + 1), Point(bx, b.rect.bottom() - 1));
		} else {
			dir = Point(0, 1);
			int ay = a.rect.bottom();
			int by = b.rect.top();
			src = FindDoorPosition(Point(a.rect.left() + 1, ay), Point(a.rect.right() - 1, ay));
			dst = FindDoorPosition(Point(b.rect.left() + 1, by), Point(b.rect.right() - 1, by));
		}
		if(v && tile(src+dir) != WALL)
			return;
		if(okdoor(src) || !v)
			MakeDoor(src);

		if(!DigCorridor(src + dir, dst, v))
			return;
		if(okdoor(dst))
			MakeDoor(dst);

		if(a.link < b.link)
			b.link = a.link;
		else
			a.link = b.link;
	}

	bool okdig(Point pt)
	{
		auto type = tile(pt);
		return type == CORRIDOR || type == WALL;
	}

	void MakeDoor(Point pt)
	{
		for(auto delta: Rect(Point(-1,-1), Size(3,3)))
			if(tile(pt + delta) == DOOR && rn2(3))
				return;
		tile(pt) = DOOR;
	}

	bool DigCorridor(Point src, Point& dst, bool v)
	{
		size_t count = 0;
		Point delta = Point(src.x > dst.x ? -1 : src.x < dst.x, src.y > dst.y ? -1 : src.y < dst.y);
		if(delta.x) delta.y = 0;
		if(!okdig(src))
			return false;
		tile(src) = CORRIDOR;
		while(true) {
			if(count++ > 500 || v && !rn2(35))
				return false;
			src = src + delta;
			if(src == dst)
				return true;
			if(tile(src) != WALL)
				return false;
			if(!rect.contains(src) || src.x == rect.left() || src.x == rect.right() || src.y == rect.top() || src.y == rect.bottom())
				return false;
			tile(src) = CORRIDOR;
			auto diff = dst - src;
			if(abs(diff.width) + abs(diff.height) <= 1)
				return true;

			int dix = std::abs(src.x - dst.x);
			int diy = std::abs(src.y - dst.y);
			if((dix > diy) && diy && !rn2(dix - diy + 1)) {
				dix = 0;
			} else if((diy > dix) && dix && !rn2(diy - dix + 1)) {
				diy = 0;
			}

			if(delta.y && dix > diy) {
				int ddx = (src.x > dst.x) ? -1 : 1;
				if(okdig(src + Point(ddx, 0))) {
					delta = Point(ddx, 0);
					continue;
				}
			} else if(delta.x && diy > dix) {
				int ddy = (src.y > dst.y) ? -1 : 1;

				if(okdig(src + Point(0, ddy))) {
					delta = Point(0, ddy);
					continue;
				}
			}
			if(okdig(src + delta))
				continue;

			if(delta.x) {
				delta = Point(0, dst.y < src.y ? -1 : 1);
			} else {
				delta = Point(dst.x < src.x ? -1 : 1, 0);
			}
			if(okdig(src + delta))
				continue;
			delta = Point(-delta.x, -delta.y);
		}
	}


	// [0..n] equal probability
	size_t rn2(size_t n) { return std::uniform_int_distribution<size_t>(0, n)(rng); }
	// Choose one element with equal probability
	template<class T>
	typename std::vector<T>::iterator rn2(std::vector<T>& list) { return list.begin() + rn2(list.size() - 1); }
	// [low..high] equal probability
	int rnd(int low, int high) {
		if(low >= high)
			return low;
		return std::uniform_int_distribution<int>(low, high)(rng);
	}

	std::mt19937 rng;
	Rect rect;
	Size room_minimum_size;
	Size room_maximum_size;
	std::vector<Rect> rects;
	std::vector<Room> rooms;
	std::vector<TileType> tiles;
	std::set<std::pair<int, int>> joins;
};

class RoomAndCorridorLevel : public Level
{
public:
	static Level* Create(std::mt19937& rng)
	{
		auto level = new RoomAndCorridorLevel();
		level->Init(rng);
		return level;
	}

private:
	void Init(std::mt19937& rng)
	{
		MapGenerator mapgen(rng, Rect(Point(1,1), Point(78, 22)));
		mapgen.GenerateRooms(12);
		mapgen.GenerateCorridors();

		for(size_t i = 0; i < mapgen.get_tiles().size(); i++) {
			auto tile = mapgen.get_tiles()[i];
			int x = 0;
			int y = 0;
		}

		for(auto pt: mapgen.rect) {
			auto& t = map->tile(pt)->terrain;
			switch(mapgen.get_tiles()[(pt.x - mapgen.rect.tl.x) + (pt.y - mapgen.rect.tl.y) * mapgen.rect.size.width]) {
			case MapGenerator::CORRIDOR:
				if(!mapgen.rn2(50))
					t = terrain_secret_corridor;
				else
					t = terrain_corridor;
				break;
			case MapGenerator::ROOM_FLOOR:
				t = terrain_floor;
				break;
			case MapGenerator::ROOM_HWALL:
				t = terrain_wall_horizontal;
				break;
			case MapGenerator::ROOM_VWALL:
				t = terrain_wall_vertical;
				break;
			case MapGenerator::DOOR:
				t = terrain_corridor;
				break;
			case MapGenerator::WALL:
				t = terrain_rock;
				break;
			}
		}

		auto upstair_room = mapgen.rn2(mapgen.rooms);
		auto downstair_room = mapgen.rn2(mapgen.rooms);
		// Try to avoid having the stairs in adjacent or the same room
		for(int i = 0; i < 5 && abs(upstair_room - downstair_room) <= 1; i++)
			downstair_room = mapgen.rn2(mapgen.rooms);
		auto rnd = std::bind(&MapGenerator::rnd, &mapgen, std::placeholders::_1, std::placeholders::_2);
		upstair_point = upstair_room->random_point(rnd);
		map->tile(upstair_point)->terrain = terrain_upstair;
		downstair_point = downstair_room->random_point(rnd);
		map->tile(downstair_point)->terrain = terrain_downstair;

	}
};

int main()
{
	LH_initialize(LH_Renderer_DEFAULT);
	Window *window = CreateWindowed("Hello HackWorld!", 80, 25, 16, 16);
	if(!window) {
		printf("Can't create SDL GL window\n");
		return 1;
	}

	// Create a standard rougelike console
	Console *map_console = libhack::CreateConsole(80, 22, libhack::Console::FIXED);
	// Create a console just for messages
	Console *messages = libhack::CreateConsole(80, 3, libhack::Console::TERMINAL);

	// The default texture is 24-bit, so we need to specify the transparent color
	auto font_texture = CreateTextureFromFile("../terminal.png", &Black);
	if(!font_texture) {
		printf("Can't load font texture\n");
		return 1;
	}

	FontDefaultImpl font;
	FontSetColumnMajorASCII(&font, font_texture);
	map_console->SetFont(&font);
	messages->SetFont(&font);

	messages->SetBackground(Red);
	messages->SetForeground(White);


	// In this example we bind two consoles to one window, messages on top and the map below
	window->BindConsole(messages, Point(0, 0));
	window->BindConsole(map_console, Point(0, 3));

	std::mt19937 rng;
	rng.seed(10000);
	Level *level = RoomAndCorridorLevel::Create(rng);
	Map *map = *level;
	map->SetConsole(map_console, Point());

	PlayerCharacter player(level->upstair(), Glyph('@'), map);

	// Implement moving the @
	auto bind_move_key = [window](PlayerCharacter *e, int key, Point delta) {
		window->BindVirtual(key, [e, delta](uint32_t modifiers, int key, bool down) {
			if(down && !e->MoveDelta(delta)) e->CantMove(e->location() + delta);
		});
	};
	bind_move_key(&player, SDLK_k, Point(0, -1));
	bind_move_key(&player, SDLK_j, Point(0, 1));
	bind_move_key(&player, SDLK_l, Point(1, 0));
	bind_move_key(&player, SDLK_h, Point(-1, 0));
	bind_move_key(&player, SDLK_y, Point(-1, -1));
	bind_move_key(&player, SDLK_u, Point(1, -1));
	bind_move_key(&player, SDLK_b, Point(-1, 1));
	bind_move_key(&player, SDLK_n, Point(1, 1));

	window->BindVirtual(SDLK_PERIOD, [&player, level](uint32_t modifiers, int key, bool down) {
		if(player.location() == level->downstair()) {
			exit(0);
		}
	});

	while(!window->closed()) {
		LH_mainloop(nullptr);
	}
	return 0;
}
