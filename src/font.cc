#include "libhack/font.h"
#include "libhack/texture.h"

namespace libhack {

void FontSetRowMajorASCII(Font *font, Texture *tex)
{
	if(!tex)
		return;
	for(int y = 0; y < 16; y++)
		for(int x = 0; x < 16; x++)
			font->Add((Char)(x + y * 16), tex, (float)x / 16.0f, (float)y / 16.0f, (float)(x + 1) / 16.0f, (float)(y + 1) / 16.0f);
}

void FontSetColumnMajorASCII(Font *font, Texture *tex)
{
	if(!tex)
		return;
	for(int y = 0; y < 16; y++)
		for(int x = 0; x < 16; x++)
			font->Add((Char)(y + x * 16), tex, (float)x / 16.0f, (float)y / 16.0f, (float)(x + 1) / 16.0f, (float)(y + 1) / 16.0f);
}

}
