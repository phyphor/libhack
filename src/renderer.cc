#include "renderer.h"

namespace libhack {

void Vertex::CopyAttrib(const Vertex& v)
{
	memcpy(bg, v.bg, sizeof(bg));
	memcpy(fg, v.fg, sizeof(fg));
	memcpy(uv, v.uv, sizeof(uv));
}

void Vertex::ClearAttrib()
{
	uv[0] = uv[1] = bg[0] = bg[1] = bg[2] = bg[3] = 0;
}

namespace render {

// Describe our vertex format
gl::PackedVertexDesc vertex_desc = {
	sizeof(Vertex),
	{
		gl::PackedVertexDesc::Element(GL_FLOAT, 3, 0), // position
		gl::PackedVertexDesc::Element(GL_FLOAT, 4, 12), // background color
		gl::PackedVertexDesc::Element(GL_FLOAT, 4, 28), // foreground color
		gl::PackedVertexDesc::Element(GL_FLOAT, 2, 44), // texture coords
	},
};

const char console_vs[] = R"ZZ(
#version 150
#extension GL_ARB_explicit_attrib_location: enable
layout(location = 0) in vec3 v_pos;
layout(location = 1) in vec4 v_bg_color;
layout(location = 2) in vec4 v_fg_color;
layout(location = 3) in vec2 v_glyph_uv;

out vec4 f_bg_color;
out vec4 f_fg_color;
out vec2 f_glyph_uv;

uniform vec3 u_offset;
uniform vec3 u_scale;

void main()
{
	gl_Position = vec4((v_pos + u_offset) * u_scale, 1);
	f_bg_color = v_bg_color;
	f_fg_color = v_fg_color;
	f_glyph_uv = v_glyph_uv;
}
)ZZ";

const char console_fs[] = R"ZZ(
#version 150
#extension GL_ARB_explicit_attrib_location: enable
in vec4 f_bg_color;
in vec4 f_fg_color;
in vec2 f_glyph_uv;
uniform sampler2D u_font_page;
layout(location = 0) out vec4 color;

void main()
{
	vec4 c = texture(u_font_page, f_glyph_uv);
	float alpha = f_fg_color.a * c.a;
	color = f_fg_color * c * alpha + f_bg_color * (1 - alpha);
}
)ZZ";


class GLTexture : public Texture
{
public:
	GLTexture(unsigned int width, unsigned int height)
	{
		glGenTextures(1, &texture);
		Bind();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	~GLTexture()
	{
		glDeleteTextures(1, &texture);
	}
	void TexData(int x, int y, int w, int h, const unsigned char *pixels, bool is_rgba) override
	{
		Bind();
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, is_rgba ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, pixels);
	}
	void Bind() override
	{
		glBindTexture(GL_TEXTURE_2D, texture);
	}

private:
	GLuint texture;
};

class GLRenderBuffer : public RenderBuffer
{
public:
	GLRenderBuffer(gl::BufferObject& vbo)
	{
		vao.Setup(vbo, vertex_desc);
	}

	void Draw(const unsigned short *indices, size_t count) override
	{
		if(count == 0)
			return;
		vao.Bind();
		glDrawElements(GL_TRIANGLES, (GLsizei)count, GL_UNSIGNED_SHORT, indices);
	}

private:
	gl::VertexArray vao;
};


class GLVertexBuffer : public VertexBuffer
{
public:
	void OnChanged(const Vertex *verts, size_t count) override;
	std::unique_ptr<RenderBuffer> CreateRenderBuffer() override;

private:
	gl::BufferObject vbo;
};


void GLVertexBuffer::OnChanged(const Vertex *verts, size_t count)
{
	vbo.LoadData(GL_ARRAY_BUFFER, GL_DYNAMIC_DRAW, count * sizeof(Vertex), verts);
}

std::unique_ptr<RenderBuffer> GLVertexBuffer::CreateRenderBuffer()
{
	return std::make_unique<GLRenderBuffer>(vbo);
}

GLRenderer::GLRenderer()
{
	gl::Shader vs(GL_VERTEX_SHADER), fs(GL_FRAGMENT_SHADER);

	vs.SetSource(console_vs);
	fs.SetSource(console_fs);
	console_program.Link({&vs, &fs});

	console_program.Use();
	u_offset = glGetUniformLocation(console_program, "u_offset");
	u_scale = glGetUniformLocation(console_program, "u_scale");
	u_font_page = glGetUniformLocation(console_program, "u_font_page");
	glUniform1i(u_font_page, 0);

	glEnable(GL_TEXTURE_2D);
}

void GLRenderer::BeginRender(float offset[3], float scale[3])
{
	console_program.Use();
	glUniform3f(u_offset, offset[0], offset[1], offset[2]);
	glUniform3f(u_scale, scale[0], scale[1], scale[2]);
}

GLRenderer* GLRenderer::GetRenderer()
{
	static GLRenderer *renderer;
	if(!renderer) {
		if(!InitGLFuncs())
			abort();
		renderer = new GLRenderer();
	}
	return renderer;
}

std::unique_ptr<VertexBuffer> GLRenderer::CreateVertexBuffer()
{
	return std::make_unique<GLVertexBuffer>();
}

std::unique_ptr<Texture> GLRenderer::CreateTexture(unsigned int width, unsigned int height)
{
	return std::make_unique<GLTexture>(width, height);
}

}
}
