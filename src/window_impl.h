#ifndef LIBHACK_WINDOW_IMPL_H_
#define LIBHACK_WINDOW_IMPL_H_

#include <algorithm>
#include <functional>
#include <vector>
#include <memory>
#include <map>
#include <SDL.h>

#include "libhack/window.h"
#include  "renderer.h"

namespace libhack {

class ConsoleImpl;

class WindowImpl : public Window
{
public:
	WindowImpl()
	{
		g_windows.push_back(this);
	}
	~WindowImpl() override
	{
		g_windows.erase(std::find(g_windows.begin(), g_windows.end(), this));
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(window);
		if(g_current == this) g_current = nullptr;
	}

	void BindConsole(Console *console, Point at, Rect console_rect) override;
	void UnbindConsole(Console *console) override;
	void BindPhysicalInternal(int key, ExternalPointer<KeyEventFn> *ptr) override
	{
		physical_keymap[key] = ExternalPtr<KeyEventFn>(ptr);
	}
	void BindVirtualInternal(int key, ExternalPointer<KeyEventFn> *ptr) override
	{
		virtual_keymap[key] = ExternalPtr<KeyEventFn>(ptr);
	}

	void MakeCurrent()
	{
		if(g_current != this) {
			SDL_GL_MakeCurrent(window, context);
			g_current = this;
		}
	}

	void Swap()
	{
		SDL_GL_SwapWindow(window);
	}

	void Draw();
	void DrawContents();

	void OnKeyEvent(const SDL_KeyboardEvent& e);
	void OnInputEvent(const SDL_TextInputEvent& e);
	void OnWindowEvent(const SDL_WindowEvent& e);

	std::map<int, ExternalPtr<KeyEventFn>> physical_keymap;
	std::map<int, ExternalPtr<KeyEventFn>> virtual_keymap;

	SDL_Window *window;
	SDL_GLContext context;

	int rows, cols;
	int tile_w, tile_h;

	struct Binding
	{
		ConsoleImpl *console;
		std::unique_ptr<render::RenderBuffer> renderbuffer;
		Point at;
		Rect rect;
	};
	std::vector<Binding> bindings;

	static std::vector<WindowImpl*> g_windows;
	static WindowImpl *g_current;

	static WindowImpl* Find(SDL_Window *window)
	{
		for(auto w : g_windows)
			if(w->window == window)
				return w;
		return nullptr;
	}

	static void DrawAllWindows();
};

}

#endif
