#ifndef GL_FUNCS_H_
#define GL_FUNCS_H_

#include <SDL_opengl.h>

#ifndef CONCAT
#define CONCAT2(x, y) x ## y
#define CONCAT(x, y) CONCAT2(x, y)
#endif

#define GLFUNC(a, b) extern a CONCAT(gl, b);
#include "gl_funcs.inc.h"
#undef GLFUNC

bool InitGLFuncs();

#endif
