#include <SDL.h>
#include <stdio.h>
#include "gl_funcs.h"

#define GLFUNC(type, name) type CONCAT(gl, name);
#include "gl_funcs.inc.h"
#undef GLFUNC


bool InitGLFuncs()
{
#define GLFUNC(type, name) if(!(CONCAT(gl, name) = (type)SDL_GL_GetProcAddress("gl" #name))) { fprintf(stderr, "Failed to import %s\n", "gl" #name); return false; }
#include "gl_funcs.inc.h"
#undef GLFUNC
	return true;
}
