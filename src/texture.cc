#include "libhack/texture.h"
#include "third_party/lodepng.h"
#include "renderer.h"

namespace libhack {

struct TextureImpl : public Texture
{
	TextureImpl(Size size, std::unique_ptr<render::Texture> texture) : texture(std::move(texture)), texture_size(size) {}

	Size size() override { return texture_size; }

	void UploadPixels(Point start, Size size, const unsigned char *pixels, bool source_is_rgba)
	{
		texture->TexData(start.x, start.y, size.width, size.height, pixels, source_is_rgba);
	}

	void Use() override { texture->Bind(); }

	std::unique_ptr<render::Texture> texture;
	Size texture_size;
};

Texture* CreateTexture(Size size)
{
	return new TextureImpl(size, render::GLRenderer::GetRenderer()->CreateTexture(size.width, size.height));
}

Texture* CreateTextureFromFile(const char *path, const Color *colorkey)
{
	std::vector<unsigned char> in, out;
	lodepng::State state;
	unsigned int w, h;

	FILE *f = fopen(path, "rb");
	if(!f)
		return nullptr;
	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);
	in.resize(len);
	fread(&in[0], 1, in.size(), f);
	fclose(f);

	lodepng::decode(out, w, h, state, in);

	auto texture = CreateTexture(Size(w, h));
	if(!texture)
		return nullptr;
	if(colorkey) {
		uint8_t *ptr = &out[0];
		uint8_t r = colorkey->r(), g = colorkey->g(), b = colorkey->b();
		for(size_t i = 0; i < w*h; i++, ptr+=4) {
			if(ptr[0] == r && ptr[1] == g && ptr[2] == b)
				ptr[3] = 0;
		}
	}
	texture->UploadPixels(Point(), Size(w, h), out.data(), true);
	return texture;
}

}
