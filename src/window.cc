#include "window_impl.h"
#include "console_impl.h"
#include "renderer.h"

#include <algorithm>
#include <list>
#include <vector>
#include <SDL.h>

namespace libhack {

std::vector<WindowImpl*> WindowImpl::g_windows;
WindowImpl *WindowImpl::g_current = nullptr;

void WindowImpl::DrawAllWindows()
{
	for(auto w : g_windows)
		w->Draw();
}

void WindowImpl::Draw()
{
	if(closed())
		return;
	MakeCurrent();
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	DrawContents();
	Swap();
}

void WindowImpl::DrawContents()
{
	float scale[3] = {2.0f / (float)cols, -2.0f / (float)rows, 1};
	glEnable(GL_SCISSOR_TEST);
	for(auto& con : bindings) {
		// Prep offsets to be in clip space [-1,1].
		// Scissor coords are bottom-up hence the inversion
		glScissor(con.at.x * tile_w, (rows - (con.rect.size.height + con.at.y)) * tile_h,
			con.rect.size.width * tile_w, con.rect.size.height * tile_h);
		// Subtract the offset into the console to allow windowing
		float offset[3] = {con.at.x - con.rect.tl.x - (float)cols / 2.0f, con.at.y - con.rect.tl.y - (float)rows / 2.0f, 0};
		render::GLRenderer::GetRenderer()->BeginRender(offset, scale);
		con.console->PrepareDraw();
		if(!con.renderbuffer)
			con.renderbuffer = con.console->CreateRenderbuffer();
		if(con.renderbuffer)
			con.console->Draw(con.renderbuffer.get());
	}
	glDisable(GL_SCISSOR_TEST);
}

void WindowImpl::OnKeyEvent(const SDL_KeyboardEvent& e)
{
	auto iter = physical_keymap.find(e.keysym.scancode);
	if(iter != physical_keymap.end())
		(**iter->second)(e.keysym.mod, e.keysym.scancode, e.state == SDL_PRESSED);
	iter = virtual_keymap.find(e.keysym.sym);
	if(iter != virtual_keymap.end())
		(**iter->second)(e.keysym.mod, e.keysym.sym, e.state == SDL_PRESSED);
}

void WindowImpl::OnInputEvent(const SDL_TextInputEvent& e)
{
}

void WindowImpl::OnWindowEvent(const SDL_WindowEvent& e)
{
	if(e.event == SDL_WINDOWEVENT_CLOSE)
		is_closed = true;
}

void WindowImpl::BindConsole(Console *console, Point at, Rect console_rect)
{
	Binding bind;
	bind.console = (ConsoleImpl*)console;
	bind.at = at;
	bind.rect = console_rect;
	if(bind.rect.size.empty())
		bind.rect.size = bind.console->size();
	bindings.emplace_back(std::move(bind));
	bind.console->AddBinding(this);
}

void WindowImpl::UnbindConsole(Console *console)
{
	bindings.erase(std::find_if(bindings.begin(), bindings.end(),
		[console](auto& e) { return e.console == console; }));
	((ConsoleImpl*)console)->RemoveBinding(this);
}

Window* CreateWindowed(const char *title, int cols, int rows, int tile_w, int tile_h)
{
	SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, WindowImpl::g_windows.empty() ? 0 : 1);
	auto w = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		cols * tile_w, rows * tile_h, SDL_WINDOW_OPENGL);
	if(!w)
		return nullptr;
	auto ctx = SDL_GL_GetCurrentContext();
	if(!ctx)
		ctx = SDL_GL_CreateContext(w);
	if(!ctx) {
		SDL_DestroyWindow(w);
		return nullptr;
	}
	auto window = new WindowImpl();
	window->window = w;
	window->context = ctx;
	window->rows = rows;
	window->cols = cols;
	window->tile_w = tile_w;
	window->tile_h = tile_h;

	window->Draw();
	return window;
}

}
