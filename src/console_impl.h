#ifndef LIBHACK_CONSOLE_IMPL_H_
#define LIBHACK_CONSOLE_IMPL_H_

#include "libhack/console.h"
#include "libhack/window.h"
#include "libhack/font.h"
#include "renderer.h"

#include <memory>
#include <vector>

namespace libhack {

class FontImpl;
class WindowImpl;

class ConsoleImpl : public Console
{
public:
	ConsoleImpl(unsigned int width, unsigned int height, Mode mode);
	~ConsoleImpl();

	void puts(const char *str_utf8) override { while(*str_utf8) putc_impl(*str_utf8++); }
	void putc(LH_Char ch) override { putc_impl(ch); }
	void Set(Point pt, Char ch, Color foreground, Color background) override;
	void SetFont(Font *font) override { this->font = font; }
	bool MoveChar(Point from, Point to) override;
	void vprintf(const char *fmt, va_list varg) override;
	void InvertChar(Point at) override;

	void AddBinding(WindowImpl *w);
	void RemoveBinding(WindowImpl *w);

	void PrepareDraw();

	void Scroll();

	bool Inside(Point pt);

	void putc_impl(Char ch);

	std::unique_ptr<render::RenderBuffer> CreateRenderbuffer();
	void Draw(render::RenderBuffer *rb);

	Size size() override { return Size(width, height); }

	size_t Idx(Point pt) { return pt.x + pt.y * width; }

private:
	struct DrawGroup
	{
		DrawGroup(Texture *texture) : texture(texture) {}

		Texture *texture;
		std::vector<unsigned short> indices;

		void Draw(render::RenderBuffer *rb);

		// These are all logical indices (eg, 0, 1, 2), not vertex offsets
		void AddVertex(size_t index);
		void RemoveVertex(size_t index);
	};
	struct Element
	{
		Element() : ch(0), fg(0), bg(0), group(nullptr){}
		void Clear() { ch = 0; fg = bg = 0; }
		Char ch;
		Color fg, bg;
		DrawGroup *group;
	} *elements;

	DrawGroup* GetGroupFor(Texture *t);

	std::vector<std::unique_ptr<DrawGroup>> draw_groups;

	Font *font;

	Vertex *verts;
	std::unique_ptr<render::VertexBuffer> vbo;
	std::vector<WindowImpl*> bindings;

	bool dirty;

	Mode mode;
	unsigned int width, height;
};

}

#endif
