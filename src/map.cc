#include "libhack/map.h"

namespace libhack {

class MapImpl : public Map
{
public:
	MapImpl(Size size) : Map(size), console(nullptr)
	{
		map_tiles = new MapTile[size.width * size.height];
		for(auto pt: Rect(size))
			tile(pt)->location = pt;
	}
	~MapImpl()
	{
		delete[] map_tiles;
	}

	void SetConsole(Console *console, Point offset) override;
	void SetTerrainRect(Rect rect, const TerrainInfo& info) override;
	void SetTerrain(Point at, const TerrainInfo& info) override;
	void CenterConsole(Point at) override;
	void UpdateTile(MapTile *t);

private:

	Console *console;
	Rect console_rect;
};

LH_API Map* CreateMap(unsigned int width, unsigned int height)
{
	return new MapImpl(Size(width, height));
}

void MapImpl::SetConsole(Console *console, Point offset)
{
	this->console = console;
	if(!console)
		return;
	console_rect = Rect(offset, console->size());
	for(auto pt: console_rect) {
		UpdateTile(tile(pt));
	}
}

void MapImpl::CenterConsole(Point at)
{
	Size console_size = console->size();
	// Clamp for top/left
	at.x = std::max(0, at.x - console_size.width / 2);
	at.y = std::max(0, at.y - console_size.height / 2);
	// Clamp for bottom/right
	at.x = std::min(size.width - console_size.width, at.x);
	at.y = std::min(size.height - console_size.height, at.y);
	SetConsole(console, at);
}

void MapImpl::SetTerrainRect(Rect rect, const TerrainInfo& info)
{
	for(auto pt: rect) {
		auto t = tile(pt);
		if(!t)
			continue;
		t->terrain = info;
		UpdateTile(t);
	}
}

void MapImpl::SetTerrain(Point at, const TerrainInfo& info)
{
	auto t = tile(at);
	if(!t)
		return;
	t->terrain = info;
	UpdateTile(t);
}

void MapImpl::UpdateTile(MapTile *t)
{
	if(!console || !console_rect.contains(t->location))
		return;
	auto& g = t->displayed ? t->displayed->glyph() : t->terrain.glyph;
	console->Set(console_rect.rebase(t->location), g.ch, g.fg, g.bg);
}

}
