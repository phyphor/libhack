#include "console_impl.h"
#include "libhack/texture.h"

#include <algorithm>
#include <string.h>

namespace libhack {

bool IsDisplayChar(LH_Char ch)
{
	return ch > ' ';
}

ConsoleImpl::ConsoleImpl(unsigned int width, unsigned int height, Mode mode)
	: elements(new Element[width*height]), dirty(true), mode(mode), width(width), height(height)
{
	font = nullptr;
	verts = new Vertex[width * height * 4 + 1];
	memset(&verts[0], 0, 4 * width * height * sizeof(libhack::Vertex));
	size_t index = 0;
	for(unsigned int y = 0; y < height; y++) {
		for(unsigned int x = 0; x < width; x++) {
			for(int j = 0; j <= 1; j++) {
				for(int i = 0; i <= 1; i++, index++) {
					auto& v = verts[index];
					v.pos[0] = (float)(x + i);
					v.pos[1] = (float)(y + j);
					v.pos[2] = 0;
				}
			}
		}
	}
}

ConsoleImpl::~ConsoleImpl()
{
	delete[] elements;
	delete[] verts;
}

void ConsoleImpl::putc_impl(Char ch)
{
	switch(ch) {
	case '\r':
		write_cursor.x = 0;
		break;
	case '\n':
		write_cursor.x = 0;
		write_cursor.y++;
		if(write_cursor.y > (int)height)
			Scroll();
		break;
	default:
		if(write_cursor.x == width) {
			write_cursor.x = 0;
			write_cursor.y++;
		}
		if(write_cursor.y == height)
			Scroll();
		ConsoleImpl::Set(write_cursor, ch, foreground, background);
		write_cursor.x++;
		break;
	}
}

void ConsoleImpl::Scroll()
{
	write_cursor.y--;
	if(mode == FIXED)
		return;
	// Do a scroll
	dirty = true;
	for(unsigned int y = 0; y < height - 1; y++) {
		for(unsigned int x = 0; x < width; x++) {
			auto base = x + y * width;
			auto g_src = elements[base + width].group;
			auto g_dst = elements[base].group;
			if(g_src && g_dst != g_src) {
				if(g_dst)
					g_dst->RemoveVertex(base);
				g_src->AddVertex(base);
			}
			elements[base] = elements[base + width];
			for(size_t i = 0; i < 4; i++)
				verts[base * 4 + i].CopyAttrib(verts[base * 4 + i + width * 4]);
		}
	}
	int y = height - 1;
	auto base = y * width * 4;
	memset(&elements[y * width], 0, sizeof(Element) * width);
	for(unsigned int x = 0; x < width * 4; x++) {
		verts[base + x].ClearAttrib();
	}
}

void ConsoleImpl::Set(Point pt, Char ch, Color fg, Color bg)
{
	size_t idx = Idx(pt);
	auto& e = elements[idx];
	if(!IsDisplayChar(ch) || !font)
		ch = 0;
	if(e.ch == ch && e.fg == fg && e.bg == bg)
		return; // Nothing to do
	dirty = true;

	auto *v = &verts[idx * 4];
	if(e.bg != bg) {
		float fbg[4] = {bg.f_r(), bg.f_g(), bg.f_b(), bg.f_a()};
		memcpy(v[0].bg, fbg, 16);
		memcpy(v[1].bg, fbg, 16);
		memcpy(v[2].bg, fbg, 16);
		memcpy(v[3].bg, fbg, 16);
	}
	if(e.fg != fg) {
		float ffg[4] = {fg.f_r(), fg.f_g(), fg.f_b(), fg.f_a()};
		memcpy(v[0].fg, ffg, 16);
		memcpy(v[1].fg, ffg, 16);
		memcpy(v[2].fg, ffg, 16);
		memcpy(v[3].fg, ffg, 16);
	}
	if(e.ch != ch) {
		const Font::Mapping *lookup;
		if(!ch || !(lookup = font->Lookup(ch)) || !lookup->texture) {
			ch = 0;
			v[0].uv[0] = v[0].uv[1] = 0;
			v[1].uv[0] = v[1].uv[1] = 0;
			v[2].uv[0] = v[2].uv[1] = 0;
			v[3].uv[0] = v[3].uv[1] = 0;
		} else {
			// Check if we are changing groups
			if(!e.group || e.group->texture != lookup->texture) {
				if(e.group)
					e.group->RemoveVertex(idx);
				e.group = GetGroupFor(lookup->texture);
				e.group->AddVertex(idx);
			}
			v[0].uv[0] = lookup->u0;
			v[0].uv[1] = lookup->v0;
			v[1].uv[0] = lookup->u1;
			v[1].uv[1] = lookup->v0;
			v[2].uv[0] = lookup->u0;
			v[2].uv[1] = lookup->v1;
			v[3].uv[0] = lookup->u1;
			v[3].uv[1] = lookup->v1;
		}
	}
	// In case there is no glyph but there is a background, use a generic group
	if(!e.group && !bg.is_black()) {
		e.group = draw_groups.empty() ? GetGroupFor(nullptr) : draw_groups.front().get();
		e.group->AddVertex(idx);
	}
	e.ch = ch;
	e.bg = bg;
	e.fg = fg;
}


void ConsoleImpl::AddBinding(WindowImpl *w)
{
	bindings.push_back(w);
}

void ConsoleImpl::RemoveBinding(WindowImpl *w)
{
	bindings.erase(std::find(bindings.begin(), bindings.end(), w));
}

void ConsoleImpl::PrepareDraw()
{
	if(!dirty) return;
	dirty = false;

	if(!vbo)
		vbo = render::GLRenderer::GetRenderer()->CreateVertexBuffer();

	vbo->OnChanged(verts, width * height * 4);
}

bool ConsoleImpl::MoveChar(Point from, Point to)
{
	if(!Inside(from) || !Inside(to))
		return false;
	auto i_from = Idx(from);
	auto i_to = Idx(to);
	if(elements[i_to].group != elements[i_from].group) {
		if(elements[i_to].group)
			elements[i_to].group->RemoveVertex(i_to);
		if(elements[i_from].group)
			elements[i_from].group->AddVertex(i_to);
	}
	elements[i_to] = elements[i_from];
	elements[i_from].Clear();
	for(int i = 0; i < 4; i++) {
		verts[i_to * 4 + i].CopyAttrib(verts[i_from * 4 + i]);
		verts[i_from * 4 + i].ClearAttrib();
	}
	dirty = true;
	return true;
}

void ConsoleImpl::vprintf(const char *fmt, va_list varg)
{
	char buf[4000];
	vsprintf_s(buf, fmt, varg);
	puts(buf);
}

bool ConsoleImpl::Inside(Point pt)
{
	return pt.x >= 0 && pt.x < (int)width && pt.y >= 0 && pt.y < (int)height;
}

std::unique_ptr<render::RenderBuffer> ConsoleImpl::CreateRenderbuffer()
{
	return vbo ? vbo->CreateRenderBuffer() : nullptr;
}

void ConsoleImpl::Draw(render::RenderBuffer *rb)
{
	for(auto& dg : draw_groups)
		dg->Draw(rb);
}

void ConsoleImpl::InvertChar(Point at)
{
	if(!Inside(at))
		return;
	auto& e = elements[Idx(at)];
	Set(at, e.ch, e.bg, e.fg);
}

ConsoleImpl::DrawGroup* ConsoleImpl::GetGroupFor(Texture *t)
{
	auto iter = std::lower_bound(draw_groups.begin(), draw_groups.end(), t,
		[](auto& a, Texture *t) { return a->texture < t; });
	if(iter != draw_groups.end() && iter->get()->texture == t)
		return iter->get();
	auto g = new DrawGroup(t);
	draw_groups.emplace(iter, g);
	return g;
}

void ConsoleImpl::DrawGroup::Draw(render::RenderBuffer *rb)
{
	if(indices.empty())
		return;
	if(texture)
		texture->Use();
	rb->Draw(indices);
}

void ConsoleImpl::DrawGroup::AddVertex(size_t index)
{
	uint16_t actual = (uint16_t)(index * 4);
	// Standard draw order: 0, 1, 2, 2, 1, 3
	indices.push_back(actual + 0);
	indices.push_back(actual + 1);
	indices.push_back(actual + 2);
	indices.push_back(actual + 2);
	indices.push_back(actual + 1);
	indices.push_back(actual + 3);
}

void ConsoleImpl::DrawGroup::RemoveVertex(size_t index)
{
	uint16_t actual = (uint16_t)(index * 4);
	for(size_t i = 0; i < indices.size(); i += 6) {
		if(indices[i] == actual) {
			size_t last = indices.size() - 6;
			for(size_t j = 0; j < 6; j++)
				indices[i + j] = indices[last + j];
			indices.resize(indices.size() - 6);
			break;
		}
	}
}

Console* CreateConsole(unsigned int width, unsigned int height, Console::Mode mode)
{
	if(width * height * 4 >= 0xFFF0) // Ensure our total vertex count doesn't exceed what we can address
		return nullptr;
	return new ConsoleImpl(width, height, mode);
}

}
