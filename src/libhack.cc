#include "libhack.h"
#include "window_impl.h"

#include <SDL.h>
#include <stdio.h>

bool LH_initialize(LH_Renderer renderer)
{
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0)
		return false;
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	return true;
}

void ProcessSDLEvent(const SDL_Event& e)
{
	switch(e.type) {
	case SDL_TEXTINPUT: {
		auto *w = libhack::WindowImpl::Find(SDL_GetWindowFromID(e.text.windowID));
		if(w)
			w->OnInputEvent(e.text);
	}break;
	case SDL_KEYUP:
	case SDL_KEYDOWN: {
		auto *w = libhack::WindowImpl::Find(SDL_GetWindowFromID(e.key.windowID));
		if(w)
			w->OnKeyEvent(e.key);
		}break;
	case SDL_WINDOWEVENT: {
		auto *w = libhack::WindowImpl::Find(SDL_GetWindowFromID(e.window.windowID));
		if(w)
			w->OnWindowEvent(e.window);
		}break;
	default:
		printf("unhandled evt %d\n", e.type);
	}
}

LH_API void LH_mainloop(LH_Callbacks *callbacks)
{
	SDL_Event evt;
	while(SDL_PollEvent(&evt)) {
		if(callbacks && callbacks->process_message)
			if(!callbacks->process_message(&evt))
				continue;
		ProcessSDLEvent(evt);
	}
	if(callbacks && callbacks->tick)
		callbacks->tick();
	libhack::WindowImpl::DrawAllWindows();
}
