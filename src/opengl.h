#ifndef OPENGL_H_
#define OPENGL_H_

#include "gl_funcs.h"

#include <vector>

namespace libhack {
namespace render {
namespace gl {

class Shader
{
public:
	Shader(GLenum type) : compiled(false), shader(glCreateShader(type)) {}
	~Shader() { glDeleteShader(shader); }

	Shader(const Shader&) = delete;
	void operator=(const Shader&) = delete;

	void SetSource(const char *src)
	{
		glShaderSource(shader, 1, &src, nullptr);
	}

	bool Compile()
	{
		if(compiled)
			return true;
		glCompileShader(shader);
		GLint success = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if(success == GL_TRUE) {
			compiled = true;
			return true;
		}
		GLint logSize = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);

		compile_error.resize(logSize);
		glGetShaderInfoLog(shader, logSize, &logSize, &compile_error[0]);
		compile_error.resize(logSize);
		return false;
	}

	operator GLuint() const { return shader; }

	const std::string& error() const { return compile_error; }

private:
	bool compiled;
	GLuint shader;
	std::string compile_error;
};

class Program
{
public:
	Program() : program(glCreateProgram()) {}
	~Program() { glDeleteProgram(program); }

	Program(const Program&) = delete;
	void operator=(const Program&) = delete;

	void Use() { glUseProgram(program); }

	bool Link(const std::vector<Shader*>& shaders)
	{
		for(auto s : shaders) {
			if(!s->Compile()) {
				link_error = "Compile error: ";
				link_error += s->error();
				return false;
			}
			glAttachShader(program, *s);
		}
		glLinkProgram(program);
		GLint isLinked = 0;
		glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
		for(auto s : shaders)
			glDetachShader(program, *s);
		if(isLinked == GL_TRUE)
			return true;
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		link_error.resize(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &link_error[0]);
		link_error.resize(maxLength);
		return false;
	}

	operator GLuint() const { return program; }

	const std::string& error() const { return link_error; }

private:
	GLuint program;
	std::string link_error;
};

class BufferObject
{
public:
	BufferObject() : last_usage(0), last_size(0)
	{
		glGenBuffers(1, &buffer);
	}
	~BufferObject()
	{
		glDeleteBuffers(1, &buffer);
	}
	BufferObject(const BufferObject&) = delete;
	void operator=(const BufferObject&) = delete;

	void Bind(GLenum target) { glBindBuffer(target, buffer); }

	void LoadData(GLenum target, GLenum usage, size_t bytes, const void *data = nullptr)
	{
		Bind(target);
		if(bytes == last_size && usage == last_usage) {
			glBufferSubData(target, 0, bytes, data);
		} else {
			glBufferData(target, bytes, data, usage);
			last_size = bytes;
			last_usage = usage;
		}
	}

	operator GLuint() const { return buffer; }

private:
	GLuint buffer;
	GLenum last_usage;
	size_t last_size;
};

// Describe a vertex format that is packed into a single buffer.
// The ordering refers to which index they get bound to for the shader, the
// offsets refer to struct byte offsets.
struct PackedVertexDesc
{
	struct Element
	{
		Element(GLenum type, GLint count, size_t offset) : type(type), count(count), offset((void*)offset) {}

		GLenum type;
		GLint count;
		void *offset;
	};

	void Add(GLenum type, GLint count, void *offset)
	{
		elements.emplace_back(Element(type, count, (size_t)offset));
	}

	void Add(GLint count, float *offset)
	{
		Add(GL_FLOAT, count, offset);
	}

	GLsizei stride;
	std::vector<Element> elements;
};

class VertexArray
{
public:
	VertexArray()
	{
		glGenVertexArrays(1, &vertex_array);
	}
	~VertexArray()
	{
		glDeleteVertexArrays(1, &vertex_array);
	}
	VertexArray(const VertexArray&) = delete;
	void operator=(const VertexArray&) = delete;

	void Bind() { glBindVertexArray(vertex_array); }

	// Prepare this vao to render data from the given vbo with the given vertex format
	void Setup(GLuint vbo, const PackedVertexDesc& desc)
	{
		glBindVertexArray(vertex_array);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		for(GLuint i = 0; i < desc.elements.size(); i++) {
			auto& e = desc.elements[i];
			glEnableVertexAttribArray(i);
			glVertexAttribPointer(i, e.count, e.type, GL_FALSE, desc.stride, e.offset);
		}
	}

private:
	GLuint vertex_array;
};

}
}
}

#endif
