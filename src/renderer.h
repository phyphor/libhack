#ifndef LIBHACK_RENDERER_H_
#define LIBHACK_RENDERER_H_

#include <memory>
#include <vector>

#include "opengl.h"

namespace libhack {
struct Vertex
{
	float pos[3];
	float bg[4];
	float fg[4];
	float uv[2];

	void CopyAttrib(const Vertex& v);
	void ClearAttrib();
};

namespace render {

class Texture
{
public:
	virtual ~Texture() {}
	virtual void Bind() = 0;
	virtual void TexData(int x, int y, int w, int h, const unsigned char *pixels, bool is_rgba) = 0;
};

class RenderBuffer
{
public:
	virtual ~RenderBuffer() {}
	virtual void Draw(const unsigned short *indices, size_t count) = 0;

	void Draw(const std::vector<unsigned short>& indices)
	{
		Draw(indices.data(), indices.size());
	}


private:
};

class VertexBuffer
{
public:
	virtual ~VertexBuffer() {}

	// Call when |verts| changes
	virtual void OnChanged(const Vertex *verts, size_t count) = 0;

	virtual std::unique_ptr<RenderBuffer> CreateRenderBuffer() = 0;
};

class Renderer
{
public:
	virtual std::unique_ptr<VertexBuffer> CreateVertexBuffer() = 0;
	virtual std::unique_ptr<Texture> CreateTexture(unsigned int width, unsigned int height) = 0;
};


class GLRenderer : public Renderer
{
public:
	GLRenderer();

	std::unique_ptr<VertexBuffer> CreateVertexBuffer() override;
	std::unique_ptr<Texture> CreateTexture(unsigned int width, unsigned int height) override;

	void BeginRender(float offset[3], float scale[3]);

	static GLRenderer* GetRenderer();

private:
	gl::Program console_program;
	GLint u_offset;
	GLint u_scale;
	GLint u_font_page;
};

}
}

#endif
