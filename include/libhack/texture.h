#ifndef LIBHACK_TEXTURE_H_
#define LIBHACK_TEXTURE_H_

#include <stdbool.h>

#include "defs.h"
#include "console.h"

namespace libhack {

class Texture
{
public:
	virtual Size size() = 0;

	virtual void UploadPixels(Point start, Size size, const unsigned char *pixels, bool source_is_rgba) = 0;

	virtual void Use() = 0;

protected:
	virtual ~Texture() {}
};

LH_API Texture* CreateTexture(Size size);

// If |colorkey| is non-null then any pixels of that value are made transparent.
LH_API Texture* CreateTextureFromFile(const char *path, const Color *colorkey = nullptr);

}

#endif
