#ifndef LIBHACK_DEFS_H_
#define LIBHACK_DEFS_H_

#include <algorithm>
#include <memory>
#include <stdint.h>

#define WITH_OPENIL 1

#ifdef LIBHACK_EXPORTS
#define LH_API __declspec(dllexport)
#else
#define LH_API 
#endif

typedef uint32_t LH_Char;

namespace libhack {

typedef uint32_t Char;

// In case there is a C++ library mismatch, use this to wrap supplied data that
// is owned by the app
template<class T>
class ExternalPointer
{
public:
	ExternalPointer() = delete;
	explicit ExternalPointer(const T& v) : val(v) {}
	ExternalPointer(const ExternalPointer&) = delete;
	void operator = (const ExternalPointer&) = delete;

	virtual ~ExternalPointer() {}
	virtual void Destroy() { delete this; }

	T& operator->() { return val; }
	T& operator*() { return val; }
	operator T&() { return ptr; }

private:
	T val;
};

template<class T>
struct ExternalDeleter
{
	void operator()(ExternalPointer<T> *p) { p->Destroy(); }
};

template<class T>
using ExternalPtr = std::unique_ptr<ExternalPointer<T>, ExternalDeleter<T>>;

template<class T> ExternalPointer<T>* MakeExternalPtr(const T& val)
{
	return new ExternalPointer<T>(val);
}

struct Size
{
	constexpr Size() : width(0), height(0) {}
	constexpr Size(int w, int h) : width(w), height(h) {}

	bool empty() const { return width == 0 || height == 0; }
	bool operator == (const Size& r) const
	{
		return width == r.width && height == r.height;
	}

	int width, height;
};


struct Point
{
	constexpr Point() : x(0), y(0) {}
	constexpr Point(int x, int y) : x(x), y(y) {}

	void operator = (const Point& r) { x = r.x; y = r.y; }

	constexpr Size operator - (const Point& r) const { return Size(x - r.x, y - r.y); }
	constexpr Point operator + (const Point& r) const { return Point(x + r.x, y + r.y); }
	Point operator + (const Size& r) const { return Point(x + r.width, y + r.height); }
	bool operator == (const Point& r) const { return x == r.x && y == r.y; }
	bool operator != (const Point& r) const { return x != r.x || y != r.y; }

	void operator += (const Point& r) { x += r.x; y += r.y; }
	void operator -= (const Point& r) { x -= r.x; y -= r.y; }

	Point min(const Point& r) const { return Point(std::min(x, r.x), std::min(y, r.y)); }
	Point max(const Point& r) const { return Point(std::max(x, r.x), std::max(y, r.y)); }

	int x;
	int y;
};

struct Rect
{
	constexpr Rect() {}
	constexpr Rect(const Point& tl, const Size& size) : tl(tl), size(size) {}
	constexpr Rect(const Point& tl, const Point& br) : tl(tl), size((br + Point(1,1)) - tl) {}
	constexpr explicit Rect(const Size& size) : tl(), size(size) {}

	bool empty() const { return size.empty(); }
	bool operator == (const Rect& r) const { return tl == r.tl && size == r.size; }

	bool contains(Point pt) const
	{
		return pt.x >= tl.x && pt.y >= tl.y && pt.x < tl.x + size.width && pt.y < tl.y + size.height;
	}

	bool contains(const Rect& r) const
	{
		return left() <= r.left() && right() >= r.right() && top() <= r.top() && bottom() >= r.bottom();
	}

	Rect add_margin(Size margin) const
	{
		return Rect(Point(tl.x - margin.width, tl.y - margin.height), Size(size.width + margin.width * 2, size.height + margin.height * 2));
	}

	bool overlaps(const Rect& r) const
	{
		bool x_overlap = false;
		if(tl.x < r.tl.x) {
			if(tl.x + size.width > r.tl.x)
				x_overlap = true;
		}
		else {
			if(r.tl.x + r.size.width > tl.x)
				x_overlap = true;
		}
		if(!x_overlap)
			return false;
		if(tl.y < r.tl.y) {
			if(tl.y + size.height > r.tl.y)
				return true;
		}
		else {
			if(r.tl.y + r.size.height > tl.y)
				return true;
		}
		return false;
	}

	int left() const { return tl.x; }
	int right() const { return tl.x + size.width - 1; }
	int top() const { return tl.y; }
	int bottom() const { return tl.y + size.height - 1; }

	Point rebase(Point pt) const { return Point(pt.x - tl.x, pt.y - tl.y); }

	struct iterator
	{
		iterator(Point pt, int min_x = 0, int max_x = 0) : at(pt), min_x(min_x), max_x(max_x) {}
		bool operator == (const iterator& it) { return at == it.at; }
		bool operator != (const iterator& it) { return at != it.at; }
		void operator++()
		{
			if(++at.x == max_x) {
				at.x = min_x;
				++at.y;
			}
		}
		operator Point() { return at; }
		Point operator*() { return at; }
		Point at;
		int min_x, max_x;
	};

	iterator begin() { return iterator(tl, tl.x, tl.x + size.width); }
	iterator end() { return iterator(Point(tl.x, tl.y + size.height)); }

	Point tl;
	Size size;
};


}

#endif
