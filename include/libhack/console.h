#ifndef LIBHACK_CONSOLE_H_
#define LIBHACK_CONSOLE_H_

#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>

#include "defs.h"
#include "libhack/font.h"

namespace libhack {

struct Color
{
	Color() = default;
	Color(uint32_t v) : packed(v) {}
	constexpr Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : packed(((uint32_t)r) | (((uint32_t)g) << 8) | (((uint32_t)b) << 16) | (((uint32_t)a) << 24)) {}
	constexpr Color(uint8_t r, uint8_t g, uint8_t b) : packed(0xFF000000 | ((uint32_t)r) | (((uint32_t)g) << 8) | (((uint32_t)b) << 16)) {}

	uint8_t r() const { return packed & 0xFF; }
	uint8_t g() const { return (packed >> 8) & 0xFF; }
	uint8_t b() const { return (packed >> 16) & 0xFF; }
	uint8_t a() const { return (packed >> 24) & 0xFF; }

	float f_r() const { return (float)r() / 255.0f; }
	float f_g() const { return (float)g() / 255.0f; }
	float f_b() const { return (float)b() / 255.0f; }
	float f_a() const { return (float)a() / 255.0f; }

	bool is_black() const { return !(packed & 0xFFFFFF); }

	operator uint32_t() const { return packed; }
	uint32_t packed;
};

constexpr Color Black = Color(0, 0, 0);
constexpr Color Red = Color(255, 0, 0);
constexpr Color Green = Color(0, 255, 0);
constexpr Color Yellow = Color(255, 255, 0);
constexpr Color Blue = Color(0, 0, 255);
constexpr Color Magenta = Color(255, 0, 255);
constexpr Color Cyan = Color(0, 255, 255);
constexpr Color White = Color(255, 255, 255);

class Console
{
public:
	enum Mode
	{
		// The console behaves as a terminal. Overrunning the last line will cause scrolling.
		// The implementation will not physically scroll until at least one character is entered
		// on the new last line if it is caused by wrapping.
		TERMINAL,
		// The console behaves as a fixed field. Overrunning the last line does not
		// cause any scrolling. This is ideal for a roguelike's map area
		FIXED,
	};
	Console() : write_cursor(0, 0), background(Black), foreground(White) {}

	void SetForeground(Color fg) { foreground = fg; }
	void SetBackground(Color bg) { background = bg; }

	// Move the default printing location
	void MoveWritePosition(Point pt) { write_cursor = pt; }

	// Printf a UTF8 string at the current location
	virtual void puts(const char *str_utf8) = 0;

	// Print a character at the current location
	virtual void putc(LH_Char ch) = 0;

	// Put a character at a specified location and color
	virtual void Set(Point pt, Char ch, Color foreground, Color background) = 0;

	// Fonts define the mapping from chars to tiles
	// It could also be used as a tileset as it is just a generic codepoint -> glyph mapping.
	virtual void SetFont(Font *font) = 0;
	// Move the character from poit=nt |from| to point |to| and clear |to|.
	// Returns false if either |from| or |to| are invalid
	virtual bool MoveChar(Point from, Point to) = 0;
	inline bool MoveChar(int x0, int y0, int x1, int y1) { return MoveChar(Point(x0, y0), Point(x1, y1)); }
	// Formatted printing
	virtual void vprintf(const char *fmt, va_list varg) = 0;

	// Swap the foreground & background colors at the given location
	virtual void InvertChar(Point at) = 0;

	virtual Size size() = 0;

	void printf(const char *fmt, ...)
	{
		va_list varg;
		va_start(varg, fmt);
		vprintf(fmt, varg);
		va_end(varg);
	}

protected:
	virtual ~Console() {}

	Point write_cursor;
	Color background;
	Color foreground;
};

LH_API Console* CreateConsole(unsigned int width, unsigned int height, Console::Mode mode);

}
#endif
