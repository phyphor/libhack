#ifndef LIBHACK_FONT_H_
#define LIBHACK_FONT_H_

#include "defs.h"

#include <unordered_map>

namespace libhack {

struct FontMapping
{
	Char ch;
	float u0, v0, u1, v1;
};

class Texture;

class Font
{
public:
	struct Mapping
	{
		Mapping() {}
		Mapping(Texture *texture, float u0, float v0, float u1, float v1) : texture(texture), u0(u0), v0(v0), u1(u1), v1(v1) {}

		Texture *texture;
		float u0, v0, u1, v1;
	};

	Font() {}
	virtual ~Font() {}

	// Add the given mapping to the font. The uv coordinates are assumed to be aligned nd specify the top left and bottom
	// right of a box. A font may take glyphs from any texture. All font textures should ensure that the pixel read at
	// uv 0,0 is transparent
	virtual void Add(Char ch, Texture *tex, float u0, float v0, float u1, float v1) = 0;
	virtual const Mapping* Lookup(Char ch) = 0;

};

class FontDefaultImpl : public Font
{
public:
	void Add(Char ch, Texture *tex, float u0, float v0, float u1, float v1) override
	{
		chars[ch] = Mapping(tex, u0, v0, u1, v1);
	}
	const Mapping* Lookup(Char ch) override
	{
		auto iter = chars.find(ch);
		return iter != chars.end() ? &iter->second : nullptr;
	}

protected:
	std::unordered_map<Char, Mapping> chars;
};

// Load default bindings
LH_API void FontSetRowMajorASCII(Font *f, Texture *tex);
LH_API void FontSetColumnMajorASCII(Font *f, Texture *tex);

}

#endif
