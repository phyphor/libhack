#ifndef LIBHACK_MAP_H_
#define LIBHACK_MAP_H_

#include "console.h"
#include "defs.h"

#include <functional>
#include <list>

namespace libhack {

// A map represents a simple game world. The tiles may have terrain and may have an occupant,
// allowing moving of creatures without needing to worry about restoring the base terrain glyphs.
// Each tile may be visible or not to control drawing, in addition it may have various properties for
// vision or pathfinding.

struct Glyph
{
	Glyph(Char ch = 0, Color fg = White, Color bg = Black) : ch(ch), bg(bg), fg(fg) {}

	Char ch;
	Color bg, fg;
};

class MapEntity;

// Movement example. Each type is a single bit
// Given walking, waterwalking, lavawalking, levitation, phasing movement types:
// A firewalker would have 11100
// A levitator would have 11110
// A phaser would have 10001
// Open terrain would block 00000
// Water would block 10001
// Lava would block 11001
// A hole would block 11101
// A wall would block 11110
struct CollisionChannels
{
	CollisionChannels(uint32_t val = 0) : channels(val) {}

	bool blocks(const CollisionChannels& movement) const { return !(~channels & movement.channels); }

	static CollisionChannels All() { return CollisionChannels(~0U); }
	static CollisionChannels AllExcept(uint32_t vals) { return CollisionChannels(~vals); }
	static CollisionChannels None() { return CollisionChannels(0U); }

	uint32_t channels;
};

struct TerrainInfo
{
	TerrainInfo() {}
	TerrainInfo(Glyph g) : glyph(g) {};
	TerrainInfo(Glyph g, CollisionChannels blocks) : glyph(g), blocks(blocks) {};

	Glyph glyph;
	CollisionChannels blocks;
};

struct MapTile
{
	Point location;
	TerrainInfo terrain;

	std::list<MapEntity*> contents;
	MapEntity *displayed;

	MapTile() : displayed(nullptr) {}

	void AddEntity(MapEntity *e)
	{
		contents.push_back(e);
	}
	bool RemoveEntity(MapEntity *e)
	{
		contents.erase(std::find(contents.begin(), contents.end(), e));
		if(displayed == e) {
			displayed = contents.empty() ? nullptr : contents.front();
			return true;
		}
		return false;
	}
};

class Map
{
public:
	Map(Size size) : size(size) {}

	// Update the console and/or map drawing offset. This forces a full update
	virtual void SetConsole(Console *console, Point offset) = 0;
	// Center the output to |at|.
	virtual void CenterConsole(Point at) = 0;

	virtual void SetTerrain(Point at, const TerrainInfo& info) = 0;
	virtual void UpdateTile(MapTile *t) = 0;

	// Sets terrain in the given rect to |g|
	virtual void SetTerrainRect(Rect rect, const TerrainInfo& info) = 0;

	MapTile* tiles() const { return map_tiles; }
	bool Inside(Point pt) const { return pt.x >= 0 && pt.x < size.width && pt.y >= 0 && pt.y < size.height; }
	MapTile* tile(Point pt) { return Inside(pt) ? &map_tiles[pt.x + pt.y * size.width] : nullptr; }

protected:
	virtual ~Map() {}

	MapTile *map_tiles;
	Size size;
};

class MapEntity
{
public:
	MapEntity(Point at, Glyph g, Map *map, int priority = 0) : display_priority(priority), glyph_(g), map(map)
	{
		tile = map->tile(at);
		tile->AddEntity(this);
		MaybeTakeControl();
	}
	virtual ~MapEntity()
	{
		if(tile && tile->RemoveEntity(this))
			map->UpdateTile(tile);
	}

	MapEntity(const MapEntity&) = delete;
	void operator=(const MapEntity&) = delete;

	virtual bool CanMoveTo(MapTile *t) { return true; }

	bool MoveTo(Point to)
	{
		if(!map->Inside(to))
			return false;
		auto dest_tile = map->tile(to);
		if(!CanMoveTo(dest_tile))
			return false;
		if(tile && tile->RemoveEntity(this))
			map->UpdateTile(tile);
		dest_tile->AddEntity(this);
		tile = dest_tile;
		MaybeTakeControl();
		return true;
	}
	bool MoveDelta(Point delta) { return MoveTo(tile->location + delta); }

	void SetBlocks(CollisionChannels channels) { blockers = channels; }

	void UpdateGlyph(Glyph g)
	{
		glyph_ = g;
		MaybeTakeControl();
	}

	Point location() const { return tile->location; }
	const Glyph& glyph() const { return glyph_; }
	bool blocks(const CollisionChannels& movement) const { return blockers.blocks(movement); }
	int priority() const { return display_priority; }

protected:
	void MaybeTakeControl()
	{
		if(glyph_.ch) {
			if(!tile->displayed || tile->displayed->priority() < display_priority) {
				tile->displayed = this;
				map->UpdateTile(tile);
			}
		} else {
			// We have no glyph. Release control
			MapEntity *best = nullptr;
			for(auto e: tile->contents) {
				if(e != this && e->glyph().ch && (!best || e->priority() > best->priority()))
					best = e;
			}
			tile->displayed = best;
			map->UpdateTile(tile);
		}
	}

	int display_priority;
	CollisionChannels blockers;
	MapTile *tile;
	Glyph glyph_;
	Map *map;
};

// A unit is an entity that enforces movement rules
class MapUnit : public MapEntity
{
public:
	MapUnit(Point at, Glyph g, Map *map, int priority = 1) : MapEntity(at, g, map, priority) {}

	bool CanMoveTo(MapTile *t) override
	{
		if(t->terrain.blocks.blocks(movement_type))
			return false;
		for(auto e: t->contents)
			if(e->blocks(movement_type))
				return false;
		return true;
	}

	// Bits set represent legal movement types for this unit. Movement is allowed
	// so long as there exists a bit that is both set here and cleared in the blocking set.
	CollisionChannels movement_type;
};

LH_API Map* CreateMap(unsigned int width, unsigned int height);

}

#endif
