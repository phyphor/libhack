#ifndef LIBHACK_WINDOW_H_
#define LIBHACK_WINDOW_H_

#include <stdbool.h>

#include "defs.h"

typedef struct LH_Console LH_Console;

#include <functional>

namespace libhack {

class Console;
typedef std::function<void(uint32_t modifiers, int key, bool down)> KeyEventFn;

class Window
{
public:
	Window() : is_closed(false) {}

	void BindPhysical(int key, const KeyEventFn& fn)
	{
		BindPhysicalInternal(key, MakeExternalPtr(fn));
	}

	void BindVirtual(int key, const KeyEventFn& fn)
	{
		BindVirtualInternal(key, MakeExternalPtr(fn));
	}

	// Bind |console| to show on this window, showing at point |at| in the physical window.
	// |console_rect| can be used to select the window shown from the console, if the size
	// is empty the console's full size is used.
	virtual void BindConsole(Console *console, Point at, Rect console_rect = Rect()) = 0;
	virtual void UnbindConsole(Console *console) = 0;

	bool closed() const { return is_closed; }

protected:
	virtual ~Window() {}
	virtual void BindPhysicalInternal(int key, ExternalPointer<KeyEventFn> *ptr) = 0;
	virtual void BindVirtualInternal(int key, ExternalPointer<KeyEventFn> *ptr) = 0;

	bool is_closed;
};

LH_API Window* CreateWindowed(const char *title, int cols, int rows, int tile_w, int tile_h);

}


#endif
