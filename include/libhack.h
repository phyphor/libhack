#ifndef LIBHACK_H_
#define LIBHACK_H_

#include <stdbool.h>

#include "libhack/defs.h"

#include "libhack/console.h"
#include "libhack/font.h"
#include "libhack/texture.h"
#include "libhack/window.h"

#include "libhack/map.h"

enum LH_Renderer
{
	LH_Renderer_DEFAULT,
};

union SDL_Event;
struct LH_Callbacks
{
	void (*tick)(); // Called once per logical frame
	// Called before any internal processing. Return false to stop processing of it
	bool (*process_message)(union SDL_Event *evt);
};

LH_API bool LH_initialize(LH_Renderer renderer);
LH_API void LH_mainloop(LH_Callbacks *callbacks);

#endif
